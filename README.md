# OddJobs

Welcome to OddJobs! Sort of like the less refined cousin of TaskRabbit. With OddJobs, you can ask for help with the most informal, weird, and random of jobs!

## Inspiration

Before attending HackReactor, I quit my previous job a month prior to the start of my cohort, to give myself time to prepare for the course. In my spare time, I thought I'd make extra money, by posting that "I am looking for odd jobs" in my city's Facebook group. People inquired about dog walking, cleaning homes, putting together an AC that they just didn't want to deal with, etc...
