## Wednesday, March 27: Slow, steady, stuck

I have been making good progress on this project's backend set up, but around
7PM, I hit an impenetrable wall. I told myself that I would stop, because I have learned that towards the end of a day of coding, I am not as sharp as when I start. Naturally. So, when I run into a problem, it's best that I just step away, rather than bash my head against the same problem over and over. I think it's a very clear indicator that it is time to make my last commits and call it a day (or at least go for a walk). I have deleted all the work that was not working properly and I am calling it a night. I know I will look at this tomorrow, with my palm to my forehead, and realize how silly my mistakes are. Despite this, it has been nice getting into building things again.


## Tuesday, April 2: Still slow, but less stuck

I have made pretty good front end progress since my last entry. It has been challenging trying to remember how I set up past projects before, but this expereince has been a great refresher on what's required when I am trying to make the frontend reach the backend. I definitely am realizing that I need to stay consistent with working on my project everyday, that way that muscle is being used and I don't forget how to use React. I am definitely moving along, but I think if I am more consistent and more deliberate with my project time, I will make faster progress.

Todays' biggest accomplishment: figuring out why the log in wasn't working. I wasn't GETting the token after sending a post request log in so I wasn't being given access to stuff.


## Saturday, April 27: Can't figure out WebSockets

In theory, websockets, seem straight forward (in theory, everything in programming seems straight forward), but then there's all these errors that I can't even fathom. I have made good progress, so far, but I keep hitting a wall and I am feeling stuck so I think it's time to pivot to a different thing for today. With a fresher mind, I will figure this out tomorrow.

My big issues: WebSocket connection is closing as soon as it opens. Need to figure out why it's closing to begin with and how  I can keep it open.

## Thursday, May 23: I kind of understand WebSockets

I have had some setbacks with my progress regarding this project. I decided to step back and learn WebSocket fundamentals from the very beginning. I think I have a good grasp on how it works, but now it is just a matter of applying it to my project. I currently have the GUI set up for the chat portion of my application, but I still need to implement the WebSocket functionality of it all. I had to change the original UI I chose from a library I found online, but I was having trouble figuring out how the custom props work, so I switched it to one that I understood more which gave me better control over it. My coming goal is to successfully implement the WebSockets. I think my plan is to map each message into the "card body" component and add a conditional regarding the user.id of the person sending the message, which will determine whether their message will be situated on the left or the right. Although, I think maybe I should set up my backend before doing all of that. For now, I celebrate the things I did accomplish! yay :D
