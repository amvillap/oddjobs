import { useEffect, useState } from "react";
import ProfilePic from "./ProfilePic";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { format } from 'date-fns';
import { useNavigate } from "react-router-dom";




const profile_pic_style = {
    width: "50px",
    height: '50px',
    borderRadius: "50%",
    marginBottom: "2px"
};

function JobList () {
    const [jobList,setJobList]= useState([]);
    const [users, setUsers] = useState(null)
    const { token } = useAuthContext();
    const navigate = useNavigate();

    const getUsers = async() => {
        try {
            const response = await fetch("http://localhost:8000/api/users/", {
                credentials: "include",
            });
            if (response.ok) {
                const data = await response.json();
                const users = data["users"]
                setUsers(users)
            }
        } catch (error) {
            console.log(error)
        }
    };

    useEffect(() => {getUsers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[token])

    useEffect(() => {
        const getJobList = async() => {
            try {
                const job_list_response= await fetch('http://localhost:8000/api/job_post');
                if(job_list_response.ok) {
                    const data = await job_list_response.json();
                    console.log("data schema:", data.job_posts);
                    setJobList(data.job_posts);
                }

            } catch (error) {
                console.log("could not retrieve jobs:", error)
            }
        };
        getJobList();
    },[]);

    // function get_profile_pic(poster_id) {
    //     for (const user of users) {
    //         if (user.id === poster_id) {
    //             return user.profile_pic
    //         }
    //     }
    // }
    return(
    <>
        <div className="d-flex justify-content-center mt-5 mb-2">
            <h1>Available jobs</h1>
        </div>
        <div className="d-flex justify-content-center mb-3">
            <form class="form-inline d-flex w-50">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>

        {/* will add this later on as a map */}

        {/* <div className=" d-flex justify-content-center">
            <iframe
                title="google-map"
                width="600"
                height="450"
                style={{ border:"0"}}
                loading="lazy"
                allowfullscreen
                referrerpolicy="no-referrer-when-downgrade"
                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCOMIFi-mkHXLzPLeIlfl7AvQ_iR23bkw8
                &q=Space+Needle,Seattle+WA">
            </iframe>
        </div> */}
        <div className="d-flex justify-content-center">
                <div class="d-flex flex-wrap container justify-content-center">
                    {jobList.map((job) => {
                        return(
                            <div class="card m-3" style={{width: '16rem'}} key={job.id}>
                            <div class="card-body">
                                {/* <img src={get_profile_pic(job.poster_id)} alt={job.poster_id} style={profile_pic_style}/> */}
                                <h5 class="card-title">{job.title}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">{ format(job.start_date, "MMMM d, yyyy") } { job.start_time}-{format(job.end_date, "MMMM d, yyyy")} {job.end_time}</h6>
                                <h6 class="card-subtitle mb-2 text-muted">${job.pay_value} {job.pay_rate}</h6>
                            </div>
                            <button
                                type="button"
                                onClick={() => navigate(`/jobs/${job.id}`)}
                                class="btn btn-light">
                                    View job
                            </button>
                        </div>
                        )
                    })}
                </div>
            </div>
    </>
    )
}

export default JobList
