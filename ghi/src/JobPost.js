import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { NavLink } from 'react-router-dom';
import { format } from 'date-fns';
import { PiWarningCircle } from "react-icons/pi";



function Map(props) {
    let no_address = "null,null";
    if (props.address !== no_address) {
        return (
            <iframe
                title='job location'
                width="400"
                height="400"
                style={{border:1}}
                loading="lazy"
                allowfullscreen
                referrerpolicy="no-referrer-when-downgrade"
                src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyCOMIFi-mkHXLzPLeIlfl7AvQ_iR23bkw8
                    &q=${props.address}`}>
            </iframe>
        )
    } else {
        return (
            <div class="card message-box">
                <div class="card-body text-center d-flex flex-column justify-content-center align-items-center">
                    <PiWarningCircle
                        size={100}/>
                    {props.user} did not add a location. Please contact them to find out oddJob location.
                </div>
            </div>
        )
    }
}

function JobPost() {
    const params = useParams();
    const job_post_id = params.job_post_id;
    const [jobPost, setJobPost] = useState();
    const [jobPosterID, setJobPosterID] = useState();
    const [jobPoster, setJobPoster] = useState();
    const [address, setAddress] = useState();
    const { token } = useAuthContext();

    // getJobPost to show for the chat title

    useEffect(() => {
        const getJobPost = async () => {
            try {
                const response = await fetch(`http://localhost:8000/api/job_post/${job_post_id}`, {
                    headers: { Authorization: `Bearer ${token}`}
                })

                if (response.ok) {
                    const data = await response.json()
                    setJobPost(data)
                    setJobPosterID(data.poster_id)
                    setAddress(data.city+","+data.state)
                } else {
                    throw new Error("failed to fetch job post data");
                }
            } catch (error) {
                console.error("Error fetching job post data:", error.message)
            }
        };
        getJobPost();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token, job_post_id])
    console.log("address:", address)


    useEffect(() => {
        const getJobPoster = async () => {
            try {
                const response = await fetch(`http://localhost:8000/api/users/${jobPosterID}`, {
                    headers: { Authorization: `Bearer ${token}`}
                })

                if (response.ok) {
                    const data = await response.json()
                    let first_name = data.first_name;
                    let last_name = data.last_name;
                    setJobPoster(first_name + " " + last_name)
                    console.log(jobPoster)
                } else {
                    throw new Error("failed to fetch job poster data");
                }
            } catch (error) {
                console.error("Error fetching job poster data:", error.message)
            }
        };
        getJobPoster();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token, jobPosterID, jobPost])


    if (jobPost && jobPoster) {
        return (
            <>
                <div id="job-post-details">
                    <div class="container ">
                        <div class="row align-items-start justify-content-center">
                            <div class="col-4">
                                <Map
                                    address={address}
                                    user={jobPoster}
                                />
                            </div>
                            <div class="col-6">
                                <div class="card">
                                    <div class="card-body">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h2 class="card-title text-center">{jobPost.title}</h2>
                                                <p class="text-center text-body-secondary">Posted by: {jobPoster} </p>
                                            </li>
                                            <li class="list-group-item">
                                                <h5 class="card-title">Start: &nbsp;{ format(jobPost.start_date, "MMMM d, yyyy")}&nbsp; {jobPost.start_time}</h5>
                                            </li>
                                            <li class="list-group-item">
                                                <h5 class="card-title">End:&nbsp; { format(jobPost.end_date, "MMMM d, yyyy") } &nbsp; {jobPost.end_time}</h5>
                                            </li>
                                            <li class="list-group-item">
                                                <h5 class="card-title">manual labor: &nbsp; {jobPost.manual_labor ? "yes":"no"}</h5>
                                            </li>
                                            <li class="list-group-item">
                                                <h5 class="card-title">car required: &nbsp; {jobPost.car_required ? "yes":"no"}</h5>
                                            </li>
                                            <li class="list-group-item">
                                                <h5 class="card-title">Pay: &nbsp; &#36; {jobPost.pay_value} &nbsp; {jobPost.pay_rate} </h5>
                                            </li>
                                            <li class="list-group-item description">
                                                <p class="card-text">{jobPost.description}</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <NavLink to="/jobs/chat" class="btn btn-secondary">Chat</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default JobPost
