import React, { useState, useEffect } from "react"
import useToken, { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

const profile_pic_style = {
    width: "50px",
    height: '50px',
    margin: "10px"
};

function ProfilePic() {
    const [profilePic, setProfilePic] = useState(null);
    const { token } = useAuthContext();

    const getPicture = async () => {
        try {
            const response = await fetch('http://localhost:8000/token', {
                credentials: "include"
            });
            if (!response.ok) {
                throw new Error('Failed to fetch profile data')
            }

            const data = await response.json();
            const account = data["account"]
            setProfilePic(account.profile_pic)
        } catch(error) {
            console.log("error", error);
        };
    }

    useEffect(() => {getPicture();
    }, [token]);

    console.log("profile pic", profilePic)

    return(
        <>
            <div className="mb-2">
                <img src={profilePic} class="rounded-circle shadow-4-strong" alt="..." style={profile_pic_style}/>
            </div>
        </>
    )
}

export default ProfilePic
