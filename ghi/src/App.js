import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Nav from './Nav';
import Welcome from './Welcome';
import SignUp from './SignUpForm';
// import Intro from './Intro';
import Login from './Login';
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import JobPostForm from './JobPostForm';
import "react-widgets/styles.css";
import JobList from './Jobs';
import JobPost from './JobPost';
import Chat from './Chat';



function App() {
  return (
    <BrowserRouter>
    <AuthProvider baseUrl={"http://localhost:8000"}>
      <Nav />
      <Routes>
        <Route path="/" element={<Welcome />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/welcome" element={<Welcome />} />
        <Route path="/login" element={<Login />} />
        <Route path="/post" element={<JobPostForm />} />
        <Route path="/jobs" element={<JobList />} />
        <Route path="/jobs/:job_post_id" element={<JobPost />} />
        <Route path="/jobs/chat" element={<Chat />} />
      </Routes>
    </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
