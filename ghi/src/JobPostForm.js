import { useEffect, useState } from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";


function JobPostForm() {
    const { token } = useAuthContext();
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [startDate, setStartDate] = useState("");
    const [startTime, setStartTime] = useState("");
    const [endDate, setEndDate] = useState("");
    const [endTime, setEndTime] = useState("");
    const [payValue, setPayValue] = useState("");
    const [payRate, setPayRate] = useState("");
    const [carReq, setCarReq] = useState(false);
    const [manualLabor, setManualLabor] = useState(false);

    const handleTitleChange = (e) => {
        const value = e.target.value;
        setTitle(value);
    }

    const handleDescriptionChange = (e) => {
        const value = e.target.value;
        setDescription(value);
    }

    const handleStartDateChange = (e) => {
        const value = e.target.value;
        setStartDate(value);
    }

    const handleStartTimeChange = (e) => {
        const value = e.target.value;
        setStartTime(value);
    }

    const handleEndDateChange = (e) => {
        const value = e.target.value;
        setEndDate(value);
    }

    const handleEndTimeChange = (e) => {
        const value = e.target.value;
        setEndTime(value);
    }

    const handlePayValueChange = (e) => {
        const value = e.target.value;
        setPayValue(value);
    }

    const handlePayRateChange = (e) => {
        const value = e.target.value;
        setPayRate(value);
    }

    const handleCarReqChange = (e) => {
        const value = e.target.value;
        setCarReq(value);
    }

    const handleManualLaborChange = (e) => {
        const value = e.target.value;
        setManualLabor(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const job_post = {
            title,
            description,
            start_date: startDate,
            start_time: startTime,
            end_date: endDate,
            end_time: endTime,
            pay_value: payValue,
            pay_rate: payRate,
            car_required: carReq,
            manual_labor: manualLabor
        };

        try {
            console.log("job post", job_post)
            const jobPostResponse = await fetch('http://localhost:8000/api/job_post', {
                method: 'POST',
                credentials: "include",
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
                body: JSON.stringify(job_post)
            });

            if (jobPostResponse.ok) {
                setTitle("");
                setDescription("");
                setStartDate("");
                setStartTime("");
                setEndDate("");
                setEndTime("");
                setPayValue("");
                setPayRate("");
                setCarReq(false);
                setManualLabor(false)

            }
        } catch (error) {
            console.log("there was an error:", error)
        }
    }
    return(
        <>
        <div className="d-flex justify-content-center m-5">
            <div class="card w-50">
                <div class="card-body">
                    <h5 class="card-title">Let's look for help:</h5>
                    <form onSubmit={handleSubmit}>
                        <div class="mb-3">
                            <label for="Job title input" class="form-label">What kind of job</label>
                            <input
                                onChange={handleTitleChange}
                                type="text"
                                class="form-control"
                                id="job title"
                                placeholder="e.g. 'Do my homework'"
                                value={title}
                            />
                        </div>
                        <div class="mb-3">
                            <label for="job description" class="form-label">Job description</label>
                            <textarea
                                onChange = {handleDescriptionChange}
                                value = {description}
                                class="form-control"
                                id="job description"
                                rows="3"></textarea>
                        </div>
                        <div className="flex space-x-2 w-full row d-flex mb-3">
                            <form class="row g-2">
                                <div class="col-auto">
                                <label for="startDateInput" class="form-label">Start date</label>
                                    <input
                                        onChange={handleStartDateChange}
                                        type="date"
                                        class="form-control"
                                        id="startDate"
                                        placeholder="format: mm/dd/yy"
                                        value={startDate}
                                    />
                                </div>
                                <div class="col-auto">
                                <label for="startTimeInput" class="form-label">Start time</label>
                                    <input
                                        onChange={handleStartTimeChange}
                                        type="time"
                                        class="form-control"
                                        id="startTime"
                                        placeholder="format: 00:00 AM"
                                        value={startTime}
                                    />
                                </div>
                            </form>
                        </div>
                        <div className="flex space-x-2 w-full row d-flex mb-3">
                            <form class="row g-2">
                                <div class="col-auto">
                                <label for="startDateInput" class="form-label">End date</label>
                                    <input
                                        onChange={handleEndDateChange}
                                        type="date"
                                        class="form-control"
                                        id="endDate"
                                        placeholder="format: mm/dd/yy"
                                        value={endDate}
                                    />
                                </div>
                                <div class="col-auto">
                                <label for="endTimeInput" class="form-label">End time</label>
                                    <input
                                        onChange={handleEndTimeChange}
                                        type="time"
                                        class="form-control"
                                        id="endTime"
                                        placeholder="format: 00:00 AM"
                                        value={endTime}
                                    />
                                </div>
                            </form>
                        </div>
                        <label for="pay value and rate" class="form-label">How much do you wanna pay</label>
                        <div class="input-group input-group-sm mb-3">
                            <span class="input-group-text mb-3">$</span>
                            <input
                                onChange= {handlePayValueChange}
                                value={payValue}
                                type="text"
                                class="form-control form-control-sm mb-3"
                                aria-label="Text input with dropdown button"
                            />
                            <span>
                                <select
                                    class="form-select form-select-sm mb-3"
                                    aria-label="Large select example"
                                    onChange= {handlePayRateChange}
                                    value={payRate}
                                >
                                    <option selected>Select pay rate</option>
                                    <option value="per day">per day</option>
                                    <option value="per job">per job</option>
                                    <option value="per hour">per hour</option>
                                </select>
                            </span>
                        </div>
                        <div className="mb-3">
                            <div class="form-check form-check-inline">
                                <input
                                    onChange={handleCarReqChange}
                                    class="form-check-input"
                                    type="checkbox"
                                    id="inlineCheckbox1"
                                    value={!carReq}
                                />
                                <label class="form-check-label" for="inlineCheckbox1">Car required</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    onChange= {handleManualLaborChange}
                                    class="form-check-input"
                                    type="checkbox"
                                    id="inlineCheckbox2"
                                    value={!manualLabor}
                                />
                                <label class="form-check-label" for="inlineCheckbox2">Manual labor</label>
                            </div>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Submit"></input>
                        </form>
                    </div>
            </div>
        </div>
        </>
    )
}

export default JobPostForm
