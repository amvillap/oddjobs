import React from 'react';
import './Chat.css'
import ProfilePic from './ProfilePic';


function MessageRow(props) {
  const when = new Date(props.message.timestamp);
  return (
    // <tr>
    //   <td>{props.message.client_id}</td>
    //   <td>{when.toLocaleString()}</td>
    //   <td>{props.message.content}</td>
    // </tr>
    <li class="d-flex mb-4">
    <ProfilePic />
    <div class="card">
      <div class="card-header d-flex justify-content-between p-3">
        <p class="fw-bold mb-0">{props.message.client_id}</p>
        <p class="text-muted small mb-0"><i class="far fa-clock"></i>{when.toLocaleString()}</p>
      </div>
      <div class="card-body">
        <p class="mb-0">{props.message.content}</p>
      </div>
    </div>
    </li>
  )
}


class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      clientId: Number.parseInt(Math.random() * 10000000),
      connected: false,
      message: '',
    };
    this.sendMessage = this.sendMessage.bind(this);
    this.updateMessage = this.updateMessage.bind(this);
  }

  connect() {
    if (this.loading && !this.state.connected) {
      return;
    }
    this.loading = true;
    // Should be an environment variable in the future
    const url = `ws://localhost:8000/chat/${this.state.clientId}`;
    this.socket = new WebSocket(url);
    this.socket.addEventListener('open', () => {
      this.setState({ connected: true });
      this.loading = false;
    });
    this.socket.addEventListener('close', () => {
      this.setState({ connected: false });
      this.loading = false;
      setTimeout(() => {
        this.connect();
      }, 1000);
    });
    this.socket.addEventListener('error', () => {
      this.setState({ connected: false });
      this.loading = false;
      setTimeout(() => {
        this.connect();
      }, 1000);
    });
    this.socket.addEventListener('message', message => {
      this.setState({
        messages: [
          ...this.state.messages,
          JSON.parse(message.data)
        ],
      });
    });
  }

  componentDidMount() {
    this.connect();
  }

  sendMessage(e) {
    e.preventDefault();
    this.socket.send(this.state.message);
    this.setState({ message: '' });
  }

  updateMessage(e) {
    this.setState({ message: e.target.value });
  }

  render() {
    return (
      <>
        <h2 className="chat-id">Your ID: {this.state.clientId}</h2>
        <div class="position-relative">
            <div className="chat-container">
                    <ul class="list-unstyled">
                        <div className="messages">
                            {this.state.messages.map(message => (
                            <MessageRow
                                id={"scrollspyHeading" + this.state.messages.indexOf(message)}
                                key={message.clientId + message.timestamp}
                                message={message} />
                            ))}
                        </div>
                        <div className="input-box">
                            <li class="bg-white mb-3">
                                <div data-mdb-input-init class="form-outline">
                                    <textarea
                                        value={this.state.message}
                                        class="form-control bg-body-tertiary"
                                        id="messageText"
                                        onChange={this.updateMessage}
                                        type="text"
                                        rows="2"
                                        placeholder='Type message here...'>
                                    </textarea>
                                </div>
                                <button
                                    type="button"
                                    data-mdb-button-init data-mdb-ripple-init class="btn btn-info btn-rounded float-end"
                                    onClick={this.sendMessage}
                                    disabled={!this.state.connected}>
                                        Send
                                </button>
                            </li>
                        </div>
                    </ul>
                </div>
            </div>
      </>
    )
  }
}

export default Chat;
