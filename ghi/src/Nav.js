import { NavLink, useNavigate, Link } from 'react-router-dom';
import { useEffect } from "react";
import useToken, { useAuthContext} from "@galvanize-inc/jwtdown-for-react";


function Nav(){
    const { token } = useAuthContext();
    const { logout } = useToken();
    const navigate = useNavigate();

    const getToken = async () => {
        try {
            const response = await fetch("http://localhost:8000/token", {
                credentials: "include"
            });

            if (!response.ok) {
                throw new Error("Failed to fetch token data");
            }

            const data = await response.json();
            console.log("data", data)
        } catch (error) {
            console.log("Failed to fetch profile")
        }
    };

    useEffect(() => {
        getToken();
    }, [token]);

    const signOut = async () => {
        try {
            logout();
            if(!token) {
                navigate("/login")
            }
            console.log("successfully logged out!")
        } catch (error) {
            console.log("Failed to sign out")
        }
    }
    return(
        <nav class="navbar bg-body-tertiary">
            <div class="container-fluid">
                <Link to="/welcome" className="logo">OddJobs</Link>
                {!token && (
                    <Link to="/login" className="login-button">Log in</Link>
                )}
                {token && (
                    <>
                    <div className="d-flex justify-content-between">
                        <NavLink className="m-2" to="/jobs">Find jobs</NavLink>
                        <NavLink className="m-2" to="/post">Post a job</NavLink>
                        <NavLink className="m-2" onClick={() => signOut()}>Log out</NavLink>
                    </div>
                    </>
                )}
            </div>
        </nav>
    )
}

export default Nav
