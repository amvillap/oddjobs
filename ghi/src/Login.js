import useToken, { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useEffect, useState } from "react";
import { useNavigate, Link } from "react-router-dom";

function Login() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const { login } = useToken();
    const navigate = useNavigate();
    const { token } = useAuthContext();

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log("USERNAME", username, "PASSWORD", password)
        login(username, password);
    };

    useEffect(() => {
        if (token) {
            console.log("redirecting cuz there's a token!")
            navigate("/");
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);
    return(
    <>
        <div class="container d-flex justify-content-center mt-5">
            <div class="card text-center mb-3 d-flex justify-content-center" style={{width: '25rem'}}>
                <div class="card-body">
                    <form
                        class="form-signin m-4"
                        onSubmit={(e) => handleSubmit(e)}
                        id="sign-in-user-form"
                    >
                        <h1 className="logo">OddJobs</h1>
                        <h3 class="h3 mb-3 font-weight-normal">Please sign in</h3>
                        <input
                            onChange={(e) => setUsername(e.target.value)}
                            type="text"
                            id="username"
                            name="username"
                            class="form-control m-3"
                            placeholder="Email address"
                            required
                        />
                        <input
                            onChange={(e) => setPassword(e.target.value)}
                            type="password"
                            id="password"
                            class="form-control m-3"
                            placeholder="Password"
                            required
                            name="password"
                            />
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    </form>
                </div>
            </div>
        </div>

    </>
    )
}

export default Login
