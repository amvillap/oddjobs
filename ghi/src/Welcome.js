import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { useAuthContext} from "@galvanize-inc/jwtdown-for-react";


function Welcome() {
    const [user, setUser] = useState("");
    const { token } = useAuthContext();

    const getToken = async () => {
        try {
            const response = await fetch("http://localhost:8000/token", {
                credentials: "include"
            });

            if (!response.ok) {
                throw new Error("Failed to fetch token data");
            }

            const data = await response.json();
            const account = data["account"];
            setUser(account.first_name);
        } catch (error) {
            console.log("Failed to fetch profile")
        }
    };

    useEffect(() => {
        getToken();
    }, [user, token]);

    return(
        <div className="App">
        <header className="App-header">
        {!token && (
            <>
                <h1>welcome to OddJobs!</h1>
                <Link to={"/signup"}>Sign Up Here</Link>
            </>
        )}
        {token && (
            <>
            <h1> Welcome, {user}! </h1>
            <div className="container-sm">
                <Link to={"/post"}>
                    <button type="button" class="btn btn-primary m-2">Post a job!</button>
                </Link>
                <Link to={"/jobs"}>
                    <button type="button" class="btn btn-primary m-2">Find a job!</button>
                </Link>
            </div>
            </>
        )}

        </header>
      </div>
    )
}

export default Welcome
