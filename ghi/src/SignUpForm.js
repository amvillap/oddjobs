import useToken from "@galvanize-inc/jwtdown-for-react";
import {useState} from "react";
import {useNavigate} from "react-router-dom";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";



function SignUp() {
    const [ formData, setFormData ] = useState({
        first_name:'',
        last_name: '',
        profile_pic: '',
        password: '',
        email: ''
    });
    const navigate = useNavigate();
    const { login } = useToken();
    const token = useAuthContext();

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]:value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await fetch('http://localhost:8000/api/users', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            });
            if (!response.ok) {
                throw new Error('Failed to create user.');
            }
            setFormData({
                first_name:'',
                last_name: '',
                profile_pic: '',
                password: '',
                email: ''
            });
            login(formData.username, formData.password);
            if(token) {
                navigate("/");
            }
        } catch(error) {
            console.log("error creating user", error.message)
        }
    };
    return (
      <>
        <div class="container d-flex justify-content-center mt-5">
            <div class="card text-center mb-3 d-flex justify-content-center" style={{width: '25rem'}}>
                <div class="card-body">
                    <h3 class="card-title">Sign up!</h3>
                    <form onSubmit={handleSubmit}>
                        <div class="row">
                            <div class="col">
                                <input
                                    class="form-control"
                                    placeholder="First name"
                                    onChange={handleChange}
                                    value={formData.first_name}
                                    required_type="text"
                                    name="first_name"
                                    id="first_name"
                                />
                            </div>
                            <div class="col">
                                <input
                                    class="form-control"
                                    placeholder="Last name"
                                    onChange={handleChange}
                                    value={formData.last_name}
                                    required_type="text"
                                    name="last_name"
                                    id="last_name"
                                />
                            </div>
                        </div>
                        <div class="col-12">
                            <input
                                type="text"
                                class="form-control mt-3"
                                name="email"
                                placeholder="Email"
                                onChange={handleChange}
                                value={formData.email}
                            />
                        </div>
                        <div class="col-12">
                            <input
                                type="text"
                                class="form-control mt-3"
                                id="inputAddress"
                                placeholder="Password"
                                onChange={handleChange}
                                value={formData.password}
                                name="password"
                            />
                        </div>
                        <div class="col-12">
                            <input
                                type="text"
                                class="form-control mt-3"
                                name="profile_pic"
                                placeholder="Profile picture URL"
                                onChange={handleChange}
                                value={formData.profile_pic}
                            />
                        </div>
                        <div class="form-button mt-3 d-flex flex-column">
                            <button type="submit" class="btn btn-outline-primary">Register!</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
  }

export default SignUp
