from pydantic import BaseModel
from queries.pool import pool


class UserIn(BaseModel):
    first_name: str
    last_name: str
    profile_pic: str
    password: str
    email: str


class UserOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    profile_pic: str
    email: str


class UserOutWithPassword(UserOut):
    hashed_password: str


class UserRepo:
    def record_to_user_out(self, record):
        user_dict = {
            "id": record[0],
            "first_name": record[1],
            "last_name": record[2],
            "profile_pic": record[3],
            "password": record[4],
            "email": record[5]
        }
        return user_dict

    def create_user(self, user: UserIn, hashed_password: str) -> UserOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users
                            (first_name,
                            last_name,
                            profile_pic,
                            password,
                            email)
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING
                            id,
                            first_name,
                            last_name,
                            profile_pic,
                            password,
                            email;
                        """,
                        [
                            user.first_name,
                            user.last_name,
                            user.profile_pic,
                            hashed_password,
                            user.email
                        ]
                    )
                    id = result.fetchone()[0]
                    return UserOut(
                        id=id,
                        first_name=user.first_name,
                        last_name=user.last_name,
                        profile_pic=user.profile_pic,
                        email=user.email
                    )
        except Exception as e:
            print(e)
            return {"message": "failed to create user"}

    def get_user(self, username: str) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , first_name
                        , last_name
                        , profile_pic
                        , password
                        , email
                        FROM users
                        WHERE email = %s
                        """,
                        [username]
                    )
                    record = result.fetchone()
                    return self.record_to_user_out(record)
        except Exception as e:
            print(e)
            return {"message": "could not return retrieve user"}

    def get_one_user(self, job_poster_id: int) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , first_name
                        , last_name
                        , profile_pic
                        , password
                        , email
                        FROM users
                        WHERE id = %s
                        """,
                        [job_poster_id]
                    )
                    record = result.fetchone()
                    return self.record_to_user_out(record)
        except Exception as e:
            print(e)
            return {"message": "could not return retrieve user"}

    def update_user(
            self,
            user_id: int,
            user: UserIn,
            hashed_password: str) -> UserOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE users
                        SET
                            first_name = %s,
                            last_name = %s,
                            profile_pic = %s,
                            password = %s,
                            email = %s
                        WHERE id = %s
                        """,
                        [
                            user.first_name,
                            user.last_name,
                            user.profile_pic,
                            hashed_password,
                            user.email,
                            user_id
                        ]
                    )
                    old_user = user.dict()
                    return UserOut(
                        id=user_id,
                        **old_user
                    )
        except Exception as e:
            print(e)
            return {"message": "could not update user"}

    def get_all_users(self) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , first_name
                        , last_name
                        , profile_pic
                        , email
                        FROM users
                        """
                    )
                    records = result.fetchall()
                    users_list = [
                        {
                            "id": record[0],
                            "first_name": record[1],
                            "last_name": record[2],
                            "profile_pic": record[3],
                            "email": record[4]
                        }
                        for record in records
                    ]
                    return {"users": users_list}
        except Exception as e:
            print(e)
            return {"message": "could not retrieve users"}
