from pydantic import BaseModel
from queries.pool import pool
from datetime import date, time
from typing import Optional


class JobPostIn(BaseModel):
    title: str
    description: str
    pay_rate: str
    pay_value: float
    manual_labor: bool
    car_required: bool
    start_date: Optional[date] = None
    start_time: Optional[time] = None
    end_date: Optional[date] = None
    end_time: Optional[time] = None
    city: Optional[str] = None
    state: Optional[str] = None


class JobPostOutNoPoster(BaseModel):
    id: int
    title: str
    description: str
    pay_rate: str
    pay_value: float
    manual_labor: bool
    car_required: bool
    start_date: Optional[date] = None
    start_time: Optional[time] = None
    end_date: Optional[date] = None
    end_time: Optional[time] = None
    city: Optional[str] = None
    state: Optional[str] = None


class JobPostOut(JobPostOutNoPoster):
    poster_id: int


class JobPostRepo:
    def record_to_job_post_out(self, record):
        job_post_dict = {
            "id": record[0],
            "title": record[1],
            "description": record[2],
            "pay_rate":  record[3],
            "pay_value": record[4],
            "manual_labor": record[5],
            "car_required": record[6],
            "poster_id": record[7],
            "start_date": record[8],
            "start_time": record[9],
            "end_date": record[10],
            "end_time": record[11],
            "city": record[12],
            "state": record[13]
        }
        return job_post_dict

    def create_job_post(
            self,
            poster_id: int,
            job_post: JobPostIn) -> JobPostOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO job_post
                            (title,
                            description,
                            pay_rate,
                            pay_value,
                            manual_labor,
                            car_required,
                            poster_id,
                            start_date,
                            start_time,
                            end_date,
                            end_time,
                            city,
                            state)
                        VALUES
                            (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                        RETURNING
                        id,
                        title,
                        description,
                        pay_rate,
                        pay_value,
                        manual_labor,
                        car_required,
                        poster_id,
                        start_date,
                        start_time,
                        end_date,
                        end_time,
                        city,
                        state;
                        """,
                        [
                            job_post.title,
                            job_post.description,
                            job_post.pay_rate,
                            job_post.pay_value,
                            job_post.manual_labor,
                            job_post.car_required,
                            poster_id,
                            job_post.start_date,
                            job_post.start_time,
                            job_post.end_date,
                            job_post.end_time,
                            job_post.city,
                            job_post.state
                        ]
                    )
                    record = result.fetchone()
                    return self.record_to_job_post_out(record)
        except Exception as e:
            print(e)
            return {"message": "unable to create job post!"}

    def get_job_post(self, post_id: int) -> JobPostOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , title
                        , description
                        , pay_rate
                        , pay_value
                        , manual_labor
                        , car_required
                        , poster_id
                        , start_date
                        , start_time
                        , end_date
                        , end_time
                        , city
                        , state
                        FROM job_post
                        WHERE id = %s
                        """,
                        [post_id]
                    )
                    record = result.fetchone()
                    return self.record_to_job_post_out(record)
        except Exception as e:
            print(e)
            return {"message": "was not able to retrieve job post"}

    def get_all_job_posts(self) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , title
                        , description
                        , pay_rate
                        , pay_value
                        , manual_labor
                        , car_required
                        , poster_id
                        , start_date
                        , start_time
                        , end_date
                        , end_time
                        , city
                        , state
                        FROM job_post
                        """
                    )
                    records = result.fetchall()
                    job_post_list = [
                        {
                            "id": record[0],
                            "title": record[1],
                            "description": record[2],
                            "pay_rate": record[3],
                            "pay_value": record[4],
                            "manual_labor": record[5],
                            "car_required": record[6],
                            "poster_id": record[7],
                            "start_date": record[8],
                            "start_time": record[9],
                            "end_date": record[10],
                            "end_time": record[11],
                            "city": record[12],
                            "state": record[13]
                        }
                        for record in records
                    ]
                    return {"job_posts": job_post_list}
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve job posts"}

    def update_job_post(
            self,
            post_id: int,
            job_post: JobPostIn) -> JobPostOutNoPoster:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE job_post
                        SET
                        title = %s
                        , description = %s
                        , pay_rate = %s
                        , pay_value = %s
                        , manual_labor = %s
                        , car_required = %s
                        , start_date = %s
                        , start_time = %s
                        , end_date = %s
                        , end_time = %s
                        , city = %s
                        , state = %s
                        WHERE id = %s
                        """,
                        [job_post.title,
                         job_post.description,
                         job_post.pay_rate,
                         job_post.pay_value,
                         job_post.manual_labor,
                         job_post.car_required,
                         job_post.start_date,
                         job_post.start_time,
                         job_post.end_date,
                         job_post.end_time,
                         job_post.city,
                         job_post.state,
                         post_id]
                    )
                    old_job_post = job_post.dict()
                    return JobPostOutNoPoster(id=post_id, **old_job_post)
        except Exception as e:
            print("HERE IS WHAT'S WRONG", e)
            return {"message": "Could not update job post."}

    def delete_job_post(self, post_id: int) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM job_post
                        WHERE id = %s
                        """,
                        [post_id]
                    )
                    return {"message": "Job post successfully deleted"}
        except Exception as e:
            print(e)
            return {"message": "Failed to delete job post."}
