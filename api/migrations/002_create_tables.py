steps = [
    [
        """
            ALTER TABLE job_post
            ADD start_date DATE,
            ADD start_time TIME,
            ADD end_date DATE,
            ADD end_time TIME;
        """,
        """
            DROP TABLE job_post;
        """
    ]
]
