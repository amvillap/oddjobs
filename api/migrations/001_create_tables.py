steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            first_name VARCHAR NOT NULL,
            last_name VARCHAR NOT NULL,
            profile_pic VARCHAR NOT NULL,
            password VARCHAR NOT NULL,
            email VARCHAR NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """
    ],
    [
        """
        CREATE TYPE rate AS ENUM('per hour', 'per day', 'per job');
        CREATE TABLE job_post (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR NOT NULL,
            description TEXT NOT NULL,
            pay_rate rate NOT NULL,
            pay_value FLOAT NOT NULL,
            manual_labor BOOL,
            car_required BOOL,
            poster_id INT REFERENCES users(id)
        );
        """,
        """
        DROP TABLE job_post;
        """
    ],
    [
        """
        CREATE TABLE poster_ratings (
            id SERIAL PRIMARY KEY NOT NULL,
            job_id INT REFERENCES job_post(id),
            helper_id INT REFERENCES users(id),
            poster_id INT REFERENCES users(id),
            description TEXT NOT NULL
        );
        """,
        """
        DROP TABLE poster_ratings;
        """
    ],
    [
        """
        CREATE TABLE helper_ratings (
            id SERIAL PRIMARY KEY NOT NULL,
            job_id INT REFERENCES job_post(id),
            helper_id INT REFERENCES users(id),
            poster_id INT REFERENCES users(id),
            description TEXT NOT NULL
        );
        """,
        """
        DROP TABLE helper_ratings;
        """
    ]
]
