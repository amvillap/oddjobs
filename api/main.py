from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

import os
from routers import (
    users,
    job_post
)
from authenticator import authenticator
import chat


app = FastAPI()

app.include_router(authenticator.router)
app.include_router(users.router)
app.include_router(job_post.router)
app.include_router(chat.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
