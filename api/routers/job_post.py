from authenticator import authenticator
from fastapi import (
    APIRouter,
    Depends,
    Request,
    Response,
)
from queries.job_post import (
    JobPostIn,
    JobPostRepo,
    JobPostOut,
    JobPostOutNoPoster
)

router = APIRouter()


@router.post("/api/job_post", response_model=JobPostOut)
async def create_job_post(
    job_post: JobPostIn,
    request: Request,
    response: Response,
    repo: JobPostRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    print("account_data", account_data)
    print("job post", job_post)
    poster_id = account_data["id"]
    return repo.create_job_post(poster_id, job_post)


@router.get("/api/job_post/{post_id}", response_model=JobPostOut)
async def get_job_post(
    post_id: int,
    request: Request,
    response: Response,
    repo: JobPostRepo = Depends()
) -> JobPostOut:
    return repo.get_job_post(post_id)


@router.get("/api/job_post", response_model=dict)
async def get_all_job_posts(
    response: Response,
    repo: JobPostRepo = Depends()
):
    return repo.get_all_job_posts()


@router.put("/api/job_post/{post_id}", response_model=JobPostOutNoPoster)
async def update_job_post(
    post_id: int,
    job_post: JobPostIn,
    repo: JobPostRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    return repo.update_job_post(post_id, job_post)


@router.delete("/apo/job_post/{post_id}", response_model=dict)
async def delete_job_post(
    post_id: int,
    repo: JobPostRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
) -> dict:
    return repo.delete_job_post(post_id)
