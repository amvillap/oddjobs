from fastapi import (
    APIRouter,
    Depends,
    Request,
    Response
)
from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from queries.users import (
    UserIn,
    UserOut,
    UserRepo,
)


router = APIRouter()


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: UserOut


@router.post("/api/users", response_model=AccountToken)
async def create_user(
    user: UserIn,
    request: Request,
    response: Response,
    repo: UserRepo = Depends()
):
    hashed_password = authenticator.hash_password(user.password)
    account = repo.create_user(user, hashed_password)
    form = AccountForm(username=user.email, password=user.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.put("/api/users/{user_id}", response_model=AccountToken)
async def update_user(
    user_id: int,
    user: UserIn,
    request: Request,
    response: Response,
    repo: UserRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    hashed_password = authenticator.hash_password(user.password)
    print("hashed password", hashed_password)
    account = repo.update_user(user_id, user, hashed_password)
    print("account", account)
    form = AccountForm(username=user.email, password=user.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.get("/token", response_model=dict | None)
async def get_token(
    request: Request,
    account: UserOut = Depends(authenticator.try_get_current_account_data)
) -> AccountToken | None:
    print("account", account)
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.get("/api/users/", response_model=dict)
async def get_users(
    repo: UserRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
) -> dict:
    users_list = repo.get_all_users()
    return users_list


@router.get("/api/users/{jobPosterID}", response_model=dict)
async def get_user(
    jobPosterID: int,
    repo: UserRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
) -> dict:
    user = repo.get_one_user(jobPosterID)
    return user
